var arrayConv = function (source, nat) {
	var obyv = {
	"de": {"2008": 82217837, "2009": 82002356, "2010": 81802257, "2011": 81751602, "2012": 80327900, "2013": 80523746, "2014": 80767463, "2015": 81197537},
	"cs": {"2008": 10343422, "2009": 10425783, "2010": 10462088, "2011": 10486731, "2012": 10505445, "2013": 10516125, "2014": 10512419, "2015": 10538275},
	"sk": {"2008": 5376064, "2009": 5382401, "2010": 5390410, "2011": 5392446, "2012": 5404322, "2013": 5410836, "2014": 5415949, "2015": 5421349}
	};

	var output = [];
	for(var i in source) {
    	output.push((source[i] / obyv[nat][i]) * 100000);
	};
	return	output;
};

var draw = function(tc) {
	$.getJSON('./data/data.json', function (json) {

    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: tc
        },
        subtitle: {
            text: 'srovnání obdobných skutkových podstat'
        },
        colors: ['#b2182b', '#4575b4', '#878787'],
        xAxis: {
            categories: [
                '2008',
                '2009',
                '2010',
                '2011',
                '2012',
                '2013',
                '2014',
                '2015'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Počet případů na 100 tis. obyvatel'
            }
        },
        tooltip: {
            headerFormat: '<b>Rok {point.key}</b><br>',
            pointFormat: '{series.name}: {point.y:.1f}<br>',
            footerFormat: '<i>na 100 tis. obyvatel</i>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Česko',
            data: arrayConv(json["cs"][tc], "cs")

        }, {
            name: 'Slovensko',
            data: arrayConv(json["sk"][tc], "sk")

        }, {
            name: 'Německo',
            data: arrayConv(json["de"][tc], "de")

        }]
    });
})
};

draw("Sprejerství");